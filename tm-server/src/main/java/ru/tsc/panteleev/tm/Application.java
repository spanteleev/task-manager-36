package ru.tsc.panteleev.tm;

import ru.tsc.panteleev.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
