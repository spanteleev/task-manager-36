package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.IProjectService;
import ru.tsc.panteleev.tm.api.service.IProjectTaskService;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final User user = new User();

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @Nullable
    private final static String STRING_NULL = null;

    @After
    public void finalization() {
        taskRepository.clear();
        projectRepository.clear();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Project project = projectService.create(user.getId(), name, description);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final Task task = taskService.create(user.getId(), taskName, taskDescription);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), STRING_NULL, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), STRING_RANDOM, STRING_NULL));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), STRING_RANDOM));
        service.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertSame(project.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Project project = projectService.create(user.getId(), name, description);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final Task task = taskService.create(user.getId(), taskName, taskDescription);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), STRING_NULL, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), STRING_RANDOM, STRING_NULL));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), STRING_RANDOM));
        service.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertSame(project.getId(), task.getProjectId());
        service.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeProjectById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Project project1 = projectService.create(user.getId(), name, description);
        @NotNull final Project project2 = projectService.create(user.getId(), name, description);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final Task task1 = taskService.create(user.getId(), taskName, taskDescription);
        @NotNull final Task task2 = taskService.create(user.getId(), taskName, taskDescription);
        @NotNull final Task task3 = taskService.create(user.getId(), taskName, taskDescription);
        service.bindTaskToProject(user.getId(), project1.getId(), task1.getId());
        service.bindTaskToProject(user.getId(), project2.getId(), task2.getId());
        service.bindTaskToProject(user.getId(), project2.getId(), task3.getId());
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(user.getId(), STRING_EMPTY));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(user.getId(), STRING_NULL));
        service.removeProjectById(user.getId(), project2.getId());
        Assert.assertEquals(1, taskRepository.getSize());
        Assert.assertEquals(1, projectRepository.getSize());
    }

}
