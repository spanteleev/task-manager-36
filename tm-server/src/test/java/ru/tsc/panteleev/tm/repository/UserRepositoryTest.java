package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    private final int COUNT_TEST_USER = 666;

    @After
    public void finalization() {
        repository.clear();
    }

    public void addTestRecords() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < COUNT_TEST_USER; i++)
            users.add(new User());
        repository.add(users);
    }

    @Test
    public void add() {
        Assert.assertEquals(0L, repository.getSize());
        for (int i = 0; i < COUNT_TEST_USER; i++)
            repository.add(new User());
        Assert.assertEquals(COUNT_TEST_USER, repository.getSize());
    }

    @Test
    public void findAll() {
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_USER, repository.getSize());
        List<User> findUsers = repository.findAll();
        Assert.assertEquals(findUsers.size(), repository.getSize());
    }

    @Test
    public void findById() {
        User user = new User();
        String userId = user.getId();
        repository.add(user);
        Assert.assertNotNull(repository.findById(userId));
        Assert.assertNull(repository.findById(""));
    }

    @Test
    public void findByIndex() {
        addTestRecords();
        Assert.assertNotNull(repository.findByIndex(0));
        Assert.assertNotNull(repository.findByIndex(COUNT_TEST_USER - 1));
    }

    @Test
    public void removeById() {
        repository.add(new User());
        repository.add(new User());
        User user = new User();
        String userId = user.getId();
        repository.add(user);
        repository.removeById(userId);
        Assert.assertNull(repository.findById(userId));
        Assert.assertEquals(2L, repository.getSize());
    }

    @Test
    public void removeByIndex() {
        addTestRecords();
        repository.removeByIndex(COUNT_TEST_USER - 1);
        repository.removeByIndex(0);
        Assert.assertEquals(COUNT_TEST_USER - 2, repository.getSize());
    }

    @Test
    public void removeCollection() {
        repository.add(new User());
        List<User> users = new ArrayList<>();
        for (int i = 0; i < COUNT_TEST_USER; i++)
            users.add(new User());
        repository.add(users);
        repository.removeAll(users);
        Assert.assertEquals(1L, repository.getSize());
    }

    @Test
    public void clear() {
        addTestRecords();
        repository.clear();
        Assert.assertEquals(0L, repository.getSize());
    }

    @Test
    public void existsById() {
        User user = new User();
        String userId = user.getId();
        repository.add(user);
        Assert.assertTrue(repository.existsById(userId));
    }

    @Test
    public void findByLogin() {
        User user = new User();
        String login = UUID.randomUUID().toString();
        user.setLogin(login);
        repository.add(user);
        repository.findByLogin(login);
        Assert.assertNotNull(repository.findByLogin(login));
        Assert.assertNull(repository.findByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void findByEmail() {
        User user = new User();
        String email = UUID.randomUUID().toString();
        user.setEmail(email);
        repository.add(user);
        Assert.assertNotNull(repository.findByEmail(email));
        Assert.assertNull(repository.findByEmail(UUID.randomUUID().toString()));
    }

}
