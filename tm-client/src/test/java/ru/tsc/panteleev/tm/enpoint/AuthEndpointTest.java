package ru.tsc.panteleev.tm.enpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserProfileRequest;
import ru.tsc.panteleev.tm.dto.response.user.UserLoginResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserProfileResponse;
import ru.tsc.panteleev.tm.marker.SoapCategory;
import ru.tsc.panteleev.tm.service.PropertyService;
import java.util.UUID;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final String userLogin = "admin";

    @NotNull
    private final String userPassword = "admin";

    @Test
    public void login() {
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString())));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(userLogin, UUID.randomUUID().toString())));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(null, null)));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest(userLogin, userPassword));
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest(userLogin, userPassword));
        @NotNull final UserProfileResponse responseProfile = authEndpoint.profile(new UserProfileRequest(response.getToken()));
        Assert.assertEquals(userLogin, responseProfile.getUser().getLogin());
    }

}
