package ru.tsc.panteleev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Nullable
    private final String displayName;

    Status(@Nullable final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    @Nullable
    public static String toName(@Nullable Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

}
