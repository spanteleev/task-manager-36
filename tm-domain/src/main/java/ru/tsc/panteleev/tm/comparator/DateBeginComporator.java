package ru.tsc.panteleev.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.model.IHasDateBegin;

import java.util.Comparator;

public enum DateBeginComporator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(@Nullable IHasDateBegin o1, @Nullable IHasDateBegin o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateBegin() == null || o2.getDateBegin() == null) return 0;
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }

}
