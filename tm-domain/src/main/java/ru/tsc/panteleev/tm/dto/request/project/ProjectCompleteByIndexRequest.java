package ru.tsc.panteleev.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token);
        this.index = index;
    }

}
