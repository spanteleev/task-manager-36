package ru.tsc.panteleev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.response.AbstractResultResponse;
import ru.tsc.panteleev.tm.model.User;

@NoArgsConstructor
public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable User user) {
        super(user);
    }

}
