package ru.tsc.panteleev.tm.dto.response.project;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProjectClearResponse extends AbstractProjectResponse {

}
